"""
Modulo: archivo con extension .py, que posee su propio espacio de nombres
y puede contener variables, funciones, clases o incluso otros modulos

SIRVEN PARA: Organizar mejor el codigo y reutilizarlo
Modularizacion y reutilizacion
"""
from Ejercicios.modulos.funcionesMatematicas import sumar, multiplicar

print(sumar(5,2))
print(multiplicar(5,2))