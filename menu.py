import os

while(True):
    menu = input("\nMENU PRINCIPAL: \n1. EJERCICIOS DEL 1 AL 10 \n2. EJERCICIOS DEL 11 AL 20 \n3. EJERCICIOS DEL 20 AL 32\n4.SALIR\nESCOJA UNA OPCION: ")
    os.system('cls')
    if menu=="1":
        menu1 = input("\nEJERCICIOS DEL 1 AL 10 \n\n1. HOLA MUNDO\n2. VARIABLES EN PYTHON"
                      "\n3. CONVERSIONES\n4. NUMEROS OPERACIONES\n5. CONCATENACION\n6. CADENAS"
                      "\n7. TUPLAS\n8. LISTAS\n9. DICCIONARIOS"
                      "\n10. INGRESO DE DATOS\n*** PRESIONE CUALQUIER TECLA PARA VOLVER AL MENU PRINCIPAL ***\nESCOJA UNA OPCION: ")
        if menu1=="1":
            print("HOLA MUNDO")
        elif menu1=="2":
            print("VARIABLES EN PYTHON\n\n")
            nombre = "Dali Alcivar"
            print(nombre)

            edad = 19
            print(edad)

            edad = True
            print(edad)

            sueldo = 205.10
            print(sueldo)
        elif menu1=="3":
            print("CONVERSIONES\n\n")

            print("Concatenacion de cadena")

            numero1 = "35"
            numero2 = "18"
            print(numero1 + numero2)

            print("Conversion")

            num1 = int(numero1)
            num2 = int(numero2)
            print(num1 + num2)

            sueldo = 1200.43
            sueldoEntero = int(sueldo)
            print(sueldoEntero)

            valor = "4500.89"
            valorDecimal = float(valor)
            print(valorDecimal * 3)

            print("Longitud de caracteres")

            edad = 19
            print(len(str(edad)))
        elif menu1=="4":
            print("Numeros Operaciones")
            entero = 23
            decimal = 31.78
            complejo = 12 + 5j
            booleano = True

            print(entero)
            print(decimal)
            print(complejo)
            print(booleano)

            print("\nOperaciones Matematicas con 2 valores\n")

            num1 = 20
            num2 = 4
            sum = num1 + num2
            res = num1 - num2
            mul = num1 * num2
            div = num1 / num2
            div2 = num1 // num2
            pot = num1 ** num2

            print("La suma de", num1, "+", num2, "es igual a :", sum)
            print("La resta de", num1, "-", num2, "es igual a :", res)
            print("La multiplicacion de", num1, "*", num2, "es igual a :", mul)
            print("La division de", num1, "/", num2, "es igual a :", div)
            print("La division exacta de", num1, "/", num2, "es igual a :", div2)
            print("La potencia de", num1, "**", num2, "es igual a :", pot)
        elif menu1=="5":
            print("CONCATENACIONES")
            print("Concatenacion de variables de texto")

            print("FORMA 1")

            texto1 = "Hola"
            texto2 = "Mundo"
            textoFinal = texto1 + " " + texto2
            print(textoFinal)

            print("# FORMA 2 / USO DE COMODINES")

            print("El saludo es: %s %s" % (texto1, texto2))

            print("# FORMA 3 / FUNCION FORMAT")

            saludoFinal = "Saludo: {} {}".format(texto1, texto2)
            print(saludoFinal)

            print("# FORMA 4 / FUNCION FORMAT CON ALIAS")

            saludoFinal2 = "Saludo:  {x} {y}".format(x=texto1, y=texto2)
            print(saludoFinal2)
        elif menu1=="6":
            print("FUNCIONES DE CADENAS")
            texto = "Bienvenidos a la Tarea"
            print(texto)
            print("# Lower = Convierte cadena a minusculas")
            print(texto.lower())
            print(" Upper = Convierte cadena a mayusculas")#
            print(texto.upper())
            print("# Title = Primer caracter de cada palabra a mayusculas en la cadena")
            print(texto.title())

            print("# Find = Busca una porcion de texto que le mandemos y arroja la posicion donde se encuentra")

            print(texto.find("la"))

            print("# Count = Cuenta cuantas veces encuentra coincidencia de una letra")

            print(texto.count("e"))

            print("# Replace = Reemplaza valores en la cadena")

            textoFinal = texto.replace("e", "3")
            print(textoFinal)

            print("# isnumeric() = Comprueba si texto es numerico o no, con True/False")

            valor = texto.isnumeric()
            print(valor)

            print("# Split = Separa mediante un valor o un paraetro la cadena de texto")

            cadenaSeparada = texto.split(" ")
            print(cadenaSeparada)
        elif menu1=="7":
            print("TUPLAS\n")
            """
            Tupla = Estructura de datos propia de Python que permite almacenar distintos
            valores, son inmutables: no cambian una vez inicializados.
            """

            tupla = (1, 2, 3)
            print(tupla)

            tupla2 = (7, "Dali", True, 450.12, 16 + 7j, 15, "Felicidad", False)
            print(tupla2)

            tupla3 = (9, 3, (4, 5, 6))
            print(tupla3)

            # IMPRIMIR VALOR DONDE INDICAMOS LA POSICION EN LA TUPLA
            print(tupla2[1])

            # AL SER INMUNTABLE, SALDRA ERROR
            # tupla2[1] = "Alcivar"
            # print(tupla2)

            # ACCEDER AL ULTIMO ELEMENTO DE LA TUPLA
            print(tupla2[-1])

            # IMPRIMIR ELEMENTO DE N A N POSICION

            print(tupla2[0:4])

            # ACCEDE AL ELEMENTO DE LA TUPLA DEL FINAL AL INICIO
            print(tupla2[-2])

            # VOLCAR VALORESS EN VARIABLES

            a, b, c = tupla
            print(a)
            print(b)
            print(c)

            # GENERAR TUPLA NUEVA A PARTIR DE YA CREADAS

            tuplaFinal = tupla + tupla3
            print(tuplaFinal)

            # FUNCIION COUNT : CUENTA LOS VALORES DENTRO DE LA TUPLA

            print(tuplaFinal.count(3))

            # FUNCION INDEX : PRIMERA OCURRENCIA/POSICION DONDE ESTA UN VALOR

            print(tuplaFinal.index(3))
        elif menu1=="8":
            print("LISTAS\n")
            """
            Listas: Estructuras de datos que permiten almacenar distintos valores
            (Equivalentes a arrays en otros lenguajes de programacion=
            Son estructuras dinamicas, pueden mutar.
            """

            lista1 = ["Dali", 25, 98.3, True, "Alcivar", 56.3]
            print(lista1)

            print("IMPRIMIR TODA LA LISTA")
            print(lista1[:])

            print("# IMPRIMIR ELEMENTO EN ESPECIFICO")
            print(lista1[2])

            print("# IMPRIMIR ULTIMO ELEMENTO")
            print(lista1[-1])

            print("# ACCEDER A PORCION DE LA LISTA")

            print(lista1[0:3])
            print(lista1[:2])
            print(lista1[3:])

            print("# AÑADIR ELEMENTO AL FINAL EN LA TUPLA")
            lista1.append("Software")
            print(lista1)

            print("# INSERTAR ELEMENTO EN DETERMINADA UBICACION")

            lista1.insert(4, "Ecuador")
            print(lista1)

            print("# EXTENDER LA LISTA")

            lista1.extend(["Jose", 110, False])
            print(lista1)

            print("# POSICION DONDE SE ENCUENTRA UN ELEMENTO / INDEX")

            print(lista1.index("Alcivar"))

            print("# ELIMINAR ELEMENTO DE LA TUPLA")
            lista1.remove(56.3)
            print(lista1)

            print("# ELIMINAR ULTIMO ELEMENTO DE LA LISTA / FUNCION POP")
            lista1.pop()
            print(lista1)

            print("# UNIR LISTAS")

            lista2 = ["Milagro", "Marcos"]
            lista3 = lista1 + lista2
            print(lista3)

            print("# OPERACION MULTIPLICAR")
            print(lista2 * 4)

            print("# COMPROBAR SI ELEMENTO ESTA EN LISTA / ARROJA TRUE-FALSE")
            print("Alcivar" in lista1)
        elif menu1=="9":
            print("DICCIONARIOS\n")
            """
            Diccionarios: Estructuras de datos que nos permiten almacenar distintos valores
            Los datos se almacenanan asociados a una clave unica, tenemos una relacion clave : valor
            Los elementos almacenados estan en desorden, el orden es indiferente a la forma
            de almacenamiento
            """

            miDiccionario = {"España": "Madrid", "Peru": "Lima", "Alemania": "Berlin"}
            print(miDiccionario["Peru"])
            print(miDiccionario)

            print("# AGREGAR ELEMENTO AL DICCIONARIO")
            miDiccionario["Ecuador"] = "Quito"
            print(miDiccionario)

            print("# SI YA EXISTE LA CLAVE EN EL DICCIONARIO, SOLO SE CAMBI VALOR")
            miDiccionario["España"] = "Barcelona"
            print(miDiccionario)

            print("# ELIMINAR (DEL) VALOR ASOCIADO CON SU CLAVE")
            del miDiccionario["España"]
            print(miDiccionario)

            print("# PERMITE EL USO DE VALORES DE DISTINTOS TIPOS")
            dic2 = {"Alcivar": "Dali", 25: True, "Sueldo": 150.80}
            print(dic2[25])

            print("# SE PUEDE USAR TUPLAS PARA DEFINIR LLAVES QUE TENDRA EL DICCIONARIO")
            llaves = ("España", "Francia", "Inglaterra")
            dicPaises = {llaves[0]: "Madrid", llaves[1]: "Paris", llaves[2]: "Londres"}
            print(dicPaises)

            print("# SE PUEDE VER UN DICCIONARIO DENTRO DE OTRO DICCIONARIO")
            datosPersonales = {"Apellido": "Garcia", "Anios": {1: 2020, 2: 2021, 3: 2022}}
            print(datosPersonales)
            print(datosPersonales["Anios"])

            print("# SE PUEDE UTILIZAR FUNCION GET")
            print(datosPersonales.get("Apellido", "Oscar"))
            print(datosPersonales.get("Apellido1", "Oscar"))

            print("# SABER LLAVES Y VALORES DE DICICONARIO")
            print(datosPersonales.keys())
            print(datosPersonales.values())

            print("# GUARDAR EN LISTA Y TUPLA RESULTADO DE VALUES EN DICCIONARIO")
            valores = list(datosPersonales.values())
            print(valores)

            valores1 = tuple(datosPersonales.values())
            print(valores1)
        elif menu1=="10":
            print("INGRESO DE DATOS\n")
            nombre = "Dali"
            edad = 19

            print("Hola, " + nombre)
            print("Tu edad es: ", edad)

            nombre1 = input("Inrese su nombre: ")
            edad1 = int(input("Ingrese si edad: "))
            sueldo = float(input("Ingrese su sueldo: "))
            print("Hola, " + nombre1)
            edadFutura = edad1 + 20
            print("Tu edad es: ", edad1)
            print("Tu edad dentro de 20 años sera: ", edadFutura)
            print(nombre1, "su sueldo es: $", sueldo)

        else:
            print("")
    elif menu=="2":
        menu2 = input("\nEJERCICIOS DEL 11 AL 20 \n\n11. IF_ELSE\n12. FUNCIONES"
                      "\n13. OPERAODRS LOGICOS\n14. OPERADOR TERNARIO\n15. RANGE"
                      "\n16. FOR\n17. IF CON TUPLAS\n18. FACTORIAL"
                      "\n19. WHILE\n20. BREAK - CONTINUE - PASS\n*** PRESIONE CUALQUIER TECLA PARA VOLVER AL MENU PRINCIPAL ***\nESCOJA UNA OPCION: ")
        if menu2=="11":
            print("If_Else")
            """
            Permite hacer la validacion de una condicion, en caso sea verdadera se ejecuta un
            conjunto de intrucciones, en caso sea falsa, es opcional hacer otras
            """
            edad = int(input("Ingrese su edad: "))

            if edad > 18:
                print("Ud es mayor de edad")
            elif edad == 18:
                print("Ud tiene 18 años")
            else:
                print("Ud es menor de edad")
        elif menu2=="12":
            print("FUNCIONES")
            """
            Funcion: Conjunto de instrucciones que realizan un proceso.
            Su principal ventaja es que nos ayuda  evitar codigo repetido
            """
            def saludar():
                print("Alcivar")
                print("Dali")
                print("Unemi")
                return "Hola"
            print(saludar())
            def evaluarSueldoMinimo(sueldo):
                if sueldo >= 850:
                    print("Cumples con el sueldo minimo")
                else:
                    print("Ganas menos que el sueldo minimo")
            evaluarSueldoMinimo(5200)
        elif menu2=="13":
            print("OPERADORES LOGICOS")
            """
            AND : Y SI ADEMAS
            OR : O SI NO
            NOT : NEGACION
            """
            distancia = 400
            numeroHermanos = 3
            salarioPadres = 3000
            tieneBeneficio = False
            if (distancia > 1000 and numeroHermanos > 2) or salarioPadres < 2000:
                tieneBeneficio = True
            print(not tieneBeneficio)
            if (5 > 3 or 8 < 6):
                print("Es Verdadero")
            else:
                print("Es Falso")
        elif menu2=="14":
            print("OPERAODR TERNARIO")
            sexos = ("Hombre", "Mujer")
            posicion = True
            sexo = sexos[posicion]  # Mujer
            print(sexo)
            sexo = sexos[not posicion]  # Hombre
            print(sexo)
        elif menu2=="15":
            print("FUNCION RANGE")
            """
            Range(): Crea una lista inmutable de numeros enteros en sucesion aritmetica
            """
            numeros = range(5)  # [0,1,2,3,4]
            print(numeros[1])
            numeros1 = range(4, 10)  # [4,5,6,7,8,9]
            print(numeros1[5])
            numeros2 = range(10, 100, 8)  # De 10, hasta que sea menor a 100 e incremente de 8 en 8
            print(numeros2[9])  # [10,18,26,34,42,50,58,66,87,82...]
        elif menu2=="16":
            print("FOR")
            """
            Bucles: Estructuras de control de flujo que repiten 1 o varias lineas de codigo
            """
            for num in range(0, 20, 2):
                print("Valor actual: {0}".format(num))
            for i in range(1, 13):
                print("{} x {} es: {}".format(i, i, (i * i)))
            for nom in ["Karen", "Oscar", "Hector", "Leonardo"]:
                print("Cantidad de letras de {} es: {}".format(nom, len(nom)))
        elif menu2=="17":
            print("IF CON TUPLAS")
            print("*** CURSOS ***")
            print("Matematica - Biologia - Lenguaje - Ciencias")
            curso = input("Ingrese el curso que desea matricularse: ")
            print(curso)
            if curso in ("Matematica", "Biologia", "Lenguaje", "Ciencias"):
                print("Curso {} seleccionado".format(curso))
            else:
                print("No existe el curso que ha indicado")
        elif menu2=="18":
            print("FACTORIAL")
            """
            Factorial: Proudcto de todos los numeros positivos enteros comprendidos entre 1
            y un determinado numeros
            Factorial de 5 : 1*2*3*4*5 = 120
            """
            numero = int(input("Ingrese un numero: "))
            factorial = 1
            for n in range(1, (numero + 1)):
                factorial = factorial * n
            print("El factoria de {} es: {}".format(numero, factorial))
        elif menu2=="19":
            print("WHILE")
            """
            While: Estructura repetitiva que nos permite realizar multiples iteraciones
            basandonos en el resultado de una expresion logica que puede tener como
            resultado True o False
            """
            indice = 1
            while indice < 10:
                print("Valor actual: {}".format(indice))
                indice = indice + 1
            print("Ha finalizado el bucle While")
            # Continua el programa
            inicio1 = 2
            while inicio1 <= 100:
                print("Numero par: {}".format(inicio1))
                inicio1 += 2
            print("Ha finalizado el bucle While")
        elif menu2=="20":
            print("BREAK - CONYINUE - PASS")
            """
            Break: Salir de un bucle cuando se cumpla una condicion
            """
            print(" *** BREAK *** \n")
            for numero in range(1, 6):
                if numero == 3:
                    break
                print("El numero es: {}".format(numero))
            print("Bucle Terminado")
            """
            Continue: Omite una parte del bucle cuando se cumple una condicion y continua ocn el resto
            """
            print("\n *** CONTINUE *** ")
            for numero1 in range(1, 6):
                if numero1 == 3:
                    continue
                print("El numero es: {}".format(numero1))
            print("Bucle Terminado")
            """
            Pass : Permite continuar con una sentencia o funcion que ya no tiene o aun no tiene
            un bloque de codigo util
            """
            print("\n *** PASS *** ")
            for numero2 in range(1, 6):
                if numero2 <= 3:
                    pass
                else:
                    print("El siguiente valor es mayor a 3")
                print("El numero es: {}".format(numero2))
            print("Bucle Terminado")
            def funcionSinImplementar():
                pass
        else:
            print("")
    elif menu=="3":
        menu3 = input("\nEJERCICIOS DEL 21 AL 32 \n\n21. GENERADORES 1\n22. GENERADORES 2"
                      "\n23. EXCEPCIONES\n24. RAISE\n25. MODULOS"
                      "\n26. PAQUETES\n27. PERSONA\n28. CURSO"
                      "\n29. CUENTA\n30. HERENCIA \n31. HARENCIA MULTIPLE"
                      "\n32. POLIMORFISMO\n33. CLASES DE RELACIONES\n*** PRESIONE CUALQUIER TECLA PARA VOLVER AL MENU PRINCIPAL ***\nESCOJA UNA OPCION: ")
        if menu3=="21":
            print("GENERADORES 1")
            """
            Generadores: Concepto que nos permite extraer valores de una funcion y almacenarlo
            (de uno en uno) en objetos iterables (que se pueden recorrer), sin la necesidad de
            almacenar todos a la vez en la memoria RAM
            """
            # def generaMultiplos7(limite):
            #     numero = 1
            #     listaNumeros = []
            #
            #     while numero<=limite:
            #         listaNumeros.append(numero*7)
            #         numero = numero+1
            #     return listaNumeros
            #
            # print(generaMultiplos7(10))
            print("\nAPLICANDO CONCEPTO DE GENERADOR\n")
            def generarMultiplos7(limite):
                numero = 1
                while numero <= limite:
                    # yield = Ceder, la instruccion yield genera un objeta iiteral
                    yield numero * 7
                    numero = numero + 1
            obtieneMultiplos7 = generarMultiplos7(10)
            # print(obtieneMultiplos7)
            # for n in obtieneMultiplos7:
            #     print(n)
            # next(): Retorna el siguiente elemento de un objeto iterable:
            print(next(obtieneMultiplos7))
            print("Aqui tenemos 300 lineas de codigo")
            print(next(obtieneMultiplos7))
            print("Aqui tenemos 1000 lineas de codigo")
            print(next(obtieneMultiplos7))
            # Generadores:
            # Son mas eficientes que las funciones tradicionales
            # Muy utiles con listas de valores infinitos
            # Entre llamada y llamada, el objeto iterable entra en un estado de pausa (suspension)

        elif menu3=="22":
            print("GENERADORES 2")
            """
            Cuando se indica un asterisco antes del paramtro de una funcion, indica que se
            recibira un numero indeterminado de parametros. Que se reciben como tupla.
            """
            """
            def devuelveLenguajes(*lenguajes):
                for leng in lenguajes:
                    yield leng
            """
            def devuelveLenguajes(*lenguajes):
                for leng in lenguajes:
                    for letra in leng:
                        yield letra
            """
            def devuelveLenguajes(*lenguajes):
                for leng in lenguajes:
                    yield from leng   / Permite hacer o crear objeto iterble dentro de un elemento d eotor objeto iterable
            """
            lenguajesObtenidos = devuelveLenguajes("Python", "Java", "PHP", "Ruby", "Javascript")
            print(next(lenguajesObtenidos))
            print(next(lenguajesObtenidos))
        elif menu3=="23":
            print("EXCEPCIONES")
            """
            Excepcion: Error en tiempo de ejecucion de un programa
            """
            numero1 = 20
            numero2 = 2
            # print("La division de {} entre {} es:".format(numero1,numero2,(numero1/numero2)))
            try:
                resultado = numero1 / numero2
            # except:
            except ZeroDivisionError:
                print("No se puede dividir entre 0")
            finally:
                print("Utilizando el finally, siempre muestra lo que se ejecute")
            print("Aqui termina mi programa...")
        elif menu3=="24":
            print("RAISE")
            """
            Raise: Lanzar de forma intencional excepciones
            """
            def evaluarNota(nota):
                if nota < 0:
                    raise ValueError("Valor incorrecto")
                    # raise ZeroDivisionError("El mensaje aqui se personaliza")
                    print("Nota no válida")
                elif nota >= 16:
                    print("Excelemte")
                elif nota >= 11:
                    print("Aprobado")
                else:
                    print("No aprobo")
            evaluarNota(-5)
            print("FIN")
        elif menu3=="25":
            print("MODULOS")
            """
            Modulo: archivo con extension .py, que posee su propio espacio de nombres
            y puede contener variables, funciones, clases o incluso otros modulos

            SIRVEN PARA: Organizar mejor el codigo y reutilizarlo
            Modularizacion y reutilizacion
            """
            from modulos.funcionesMatematicas import sumar, multiplicar

            print(sumar(5, 2))
            print(multiplicar(5, 2))
        elif menu3=="26":
            print("PRUEBA PAQUETE")
            """
            Paquetes: Son directorios(carpetas) donde se almacenan modulos relacionados entre si

            SIRVEN PARA: Organizar mejor el codigo y poder reutilizarlo mejor (modularizacion y reutilizacion)

            SE CREA: Creando una carpeta o directorio con un archivo dentro llamado __init__.py

            FUNCION DEL __INIT__.PY : Convierte un directorio en modulo (paquete) que contiene
            otros modulos y esto lo hace para poder importarlos.
            """
            from Paquete1.funcionesCadena import contarLetras
            from Paquete1.funcionesNumericas import *
            print(multiplicar(5, 6))
            print(contarLetras("DaliAlcivar"))
        elif menu3=="27":
            print("CLASE PERSONA")
            """
            CONSISTE EN: Trasladar la naturalez de los objetos en la vida real a codigo de programacion.
            Los objetos de la realidad tienen caracteristicas (atributos o propiedades) y funcionalidades
            o comportamientos (funciones o metodos)

            VENTAJAS:
            - Modularizacion (division en pequeñas partes) de un programa completo.
            - Codigo fuente muy reutilizable.
            - Codigo fuente mas facil de incrementar en el futuro y mantener.
            - Si existe un fallo en una pequeña parte del codigo el programa completo no debe fallar
            necesariamente. Ademas, es mas facil de corregir tales fallos.
            - Encapsulamiento: Ocultamiento del funcionamieno interno de un objeto.
            """
            class Persona():
                # Propiedades, caracteristicas o atributos
                apellidos = ""
                nombres = ""
                edad = 0
                despierta = False
                # Funcionalidades
                def despertar(self):
                    # self : Parametro que hace referencia a la instancia perteneciente a la clase
                    self.despierta = True
                    print("Buen dia")
            persona1 = Persona()
            persona1.apellidos = "Alcivar"
            print(persona1.apellidos)
            persona1.despertar()
            print(persona1.despierta)
            print("\n")
            persona2 = Persona()
            persona2.apellidos = "Parraga"
            print(persona2.apellidos)
            print(persona2.despierta)
        elif menu3=="28":
            print("CLASE CURSO")
            class Curso():
                # nombre = "Matematica"
                # creditos = 5
                # profesion = "Ingenieria Civil"
                # Estado inicial del objeto
                def __init__(self, nom, cre, pro):
                    self.nombre = nom
                    self.creditos = cre
                    self.profesion = pro
                    self.__imparticion = "Presencial"  # Propiedad encapsulada
                def mostrarDatos(self):
                    dat = "Nombre: {} / Creditos: {} / Mode de imparticion: {}"
                    print(dat.format(self.nombre, self.creditos, self.__imparticion))
                    docenteAsignado = self.__verificarDocente()
                    if docenteAsignado:
                        print("Existe docente asignado")
                    else:
                        print("No se neceista asignar un docente")
                def __verificarDocente(self):
                    print("Verificando si existe docente asignado...")
                    if self.__imparticion == "Presencial":
                        return True
                    else:
                        return False
                # Definir lo que se mostrara cuando se llame a un objeto o funcion
                def __str__(self):
                    texto = "Nombre: {} - Crédtios: {}"
                    return texto.format(self.nombre, self.creditos)
            curso1 = Curso("Matematica", 5, "Ingenieria Civil")
            print(curso1)
            curso1.mostrarDatos()
            # curso2 = Curso("Lenguaje",4,"Ingenieria Industrial")
            # print(curso2.nombre)
        elif menu3=="29":
            print("CLASE CUENTA")
            class Cuenta():
                def __init__(self, pro, sal, mon):
                    self.__propietario = pro
                    self.__saldo = sal
                    self.__moneda = mon
                # Getters / Obtiene Valores
                def get_Saldo(self):
                    return self.__saldo
                def get_Propietario(self):
                    return self.__propietario
                def get_Moneda(self):
                    return self.__moneda
                # Setters / Modificar Valores
                def set_Moneda(self, moneda):
                    self.__moneda = moneda
            cuenta1 = Cuenta("Dali Alcivar", 15000, "Dolar")
            print(cuenta1.get_Saldo())
            print(cuenta1.get_Moneda())
            cuenta1.set_Moneda("Euro")
            print(cuenta1.get_Moneda())
        elif menu3=="30":
            print("CLASE HERENCIA")
            class Persona():
                def __init__(self, apePat, apeMat, nom):
                    self.apellidoPaterno = apePat
                    self.apellidoMaterno = apeMat
                    self.nombres = nom
                def mostrarNombreCompleto(self):
                    txt = "{} {},{}"
                    return txt.format(self.apellidoPaterno, self.apellidoMaterno, self.nombres)
                def datos(self):
                    print(self.mostrarNombreCompleto())
            class Estudiante(Persona):
                def __init__(self, apePat, apeMat, nom, pro):
                    super().__init__(apePat, apeMat, nom)
                    self.profesion = pro
                # Sobreescritura
                def datos(self):
                    super().datos()
                    print("Profesion: {}".format(self.profesion))
            estu1 = Estudiante("Torres", "Lopez", "Juan", "Ingeniera de Software")
            per1 = Persona("Alcivar", "Parraga", "Luis")
            print("HERENCIA")
            print(estu1.mostrarNombreCompleto())
            print(estu1.profesion)
            print("\nSUPER")
            estu1.datos()
            print("\nPRINCIPIO DE SUSTITUCION")
            # estu1 es una instancia de la clase Estudiante
            # print(isinstance(estu1,Estudiante))
            print(isinstance(per1, Estudiante))
        elif menu3=="31":
            print("CLASE HERENCIA MULTIPLE")
            class ClaseA():
                def __init__(self, par1, par2):
                    self.parametro1 = par1
                    self.parametro2 = par2
            class ClaseB():
                def __init__(self, par3, par4, par5):
                    self.parametro3 = par3
                    self.parametro4 = par4
                    self.parametro5 = par5
            class ClaseX(ClaseA, ClaseB):
                pass
                def __str__(self):
                    texto = "p1: {} - p2: {}"
                    return texto.format(self.parametro1, self.parametro2)
            cX1 = ClaseX(15, 21)
            print(cX1)
        elif menu3=="32":
            print("CLASE POLIMORFISMO")
            """
            Polimorfismo (poli = muchas / morfos = formas)
            """
            class Estudiante():
                def describir(self):
                    print("Soy buen estudiante")
            class Docente():
                def describir(self):
                    print("Me dedico a enseñar")
            class Trabajador():
                def describir(self):
                    print("Trabajo en un agran empresa")
            def describirPersona(persona):
                persona.describir()
            doc1 = Docente()
            describirPersona(doc1)
        elif menu3=="33":
            print("RELACIONES ENTRE CLASES")
            class Pais():
                def __init__(self, nom, pre):
                    self.nombre = nom
                    self.presidente = pre
                def __str__(self):
                    txt = "Pais: {} - Presidente: {}"
                    return txt.format(self.nombre, self.presidente)
            class Ciudad():
                def __init__(self, nom, hab, pais):
                    self.nombre = nom
                    self.habitantes = hab
                    self.pais = pais
                def __str__(self):
                    txt = "Ciudad: {} - Habitantes: {} - ({})"
                    return txt.format(self.nombre, self.habitantes, self.pais)
            class Urbanizacion():
                def __init__(self, nom, ciu):
                    self.nombre = nom
                    self.ciudad = ciu
                def __str__(self):
                    txt = "Urbanizacion: {} - ({})"
                    return txt.format(self.nombre, self.ciudad)
            pais1 = Pais("Ecuador", "Guillermo Lasso")
            print(pais1)
            ciudad1 = Ciudad("Milagro", 2544562, pais1)
            print(ciudad1)
            urb1 = Urbanizacion("Los Pinos", ciudad1)
            print(urb1)
        else:
            print("")
    elif menu=="4":
        print("GRACIAS POR USAR EL SISTEMA")
        break
    else:
        print("OPCION NO VALIDA, INGRESA UNA DE LAS OPCIONES MOSTRADAS")
print("VUELVE PRONTO")









