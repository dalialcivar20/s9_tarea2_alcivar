class Persona():
    def __init__(self,apePat,apeMat,nom):
        self.apellidoPaterno = apePat
        self.apellidoMaterno = apeMat
        self.nombres = nom

    def mostrarNombreCompleto(self):
        txt="{} {},{}"
        return txt.format(self.apellidoPaterno,self.apellidoMaterno,self.nombres)

    def datos(self):
        print(self.mostrarNombreCompleto())

class Estudiante(Persona):
    def __init__(self,apePat,apeMat,nom,pro):
        super().__init__(apePat,apeMat,nom)
        self.profesion = pro

    #Sobreescritura
    def datos(self):
        super().datos()
        print("Profesion: {}".format(self.profesion))

estu1 = Estudiante("Torres","Lopez","Juan","Ingeniera de Software")
per1 = Persona("Alcivar","Parraga","Luis")

print("HERENCIA")

print(estu1.mostrarNombreCompleto())
print(estu1.profesion)

print("\nSUPER")

estu1.datos()

print("\nPRINCIPIO DE SUSTITUCION")
# estu1 es una instancia de la clase Estudiante
#print(isinstance(estu1,Estudiante))
print(isinstance(per1,Estudiante))