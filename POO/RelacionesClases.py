class Pais():
    def __init__(self,nom,pre):
        self.nombre = nom
        self.presidente = pre

    def __str__(self):
        txt = "Pais: {} - Presidente: {}"
        return txt.format(self.nombre,self.presidente)

class Ciudad():
    def __init__(self,nom,hab,pais):
        self.nombre = nom
        self.habitantes = hab
        self.pais = pais

    def __str__(self):
        txt = "Ciudad: {} - Habitantes: {} - ({})"
        return txt.format(self.nombre,self.habitantes,self.pais)

class Urbanizacion():
    def __init__(self,nom,ciu):
        self.nombre = nom
        self.ciudad = ciu

    def __str__(self):
        txt = "Urbanizacion: {} - ({})"
        return txt.format(self.nombre,self.ciudad)




pais1=Pais("Ecuador","Guillermo Lasso")
print(pais1)

ciudad1 = Ciudad("Milagro",2544562,pais1)
print(ciudad1)

urb1 = Urbanizacion("Los Pinos",ciudad1)
print(urb1)