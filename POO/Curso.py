class Curso():
    # nombre = "Matematica"
    # creditos = 5
    # profesion = "Ingenieria Civil"

    # Estado inicial del objeto
    def __init__(self,nom,cre,pro):
        self.nombre = nom
        self.creditos = cre
        self.profesion = pro
        self.__imparticion = "Presencial" # Propiedad encapsulada

    def mostrarDatos(self):
        dat = "Nombre: {} / Creditos: {} / Mode de imparticion: {}"
        print(dat.format(self.nombre,self.creditos,self.__imparticion))
        docenteAsignado = self.__verificarDocente()
        if docenteAsignado:
            print("Existe docente asignado")
        else:
            print("No se neceista asignar un docente")

    def __verificarDocente(self):
        print("Verificando si existe docente asignado...")
        if self.__imparticion=="Presencial":
            return True
        else:
            return False

    # Definir lo que se mostrara cuando se llame a un objeto o funcion
    def __str__(self):
        texto = "Nombre: {} - Crédtios: {}"
        return texto.format(self.nombre,self.creditos)


curso1 = Curso("Matematica",5,"Ingenieria Civil")
print(curso1)
curso1.mostrarDatos()

# curso2 = Curso("Lenguaje",4,"Ingenieria Industrial")
# print(curso2.nombre)