"""
CONSISTE EN: Trasladar la naturalez de los objetos en la vida real a codigo de programacion.
Los objetos de la realidad tienen caracteristicas (atributos o propiedades) y funcionalidades
o comportamientos (funciones o metodos)

VENTAJAS:
- Modularizacion (division en pequeñas partes) de un programa completo.
- Codigo fuente muy reutilizable.
- Codigo fuente mas facil de incrementar en el futuro y mantener.
- Si existe un fallo en una pequeña parte del codigo el programa completo no debe fallar
necesariamente. Ademas, es mas facil de corregir tales fallos.
- Encapsulamiento: Ocultamiento del funcionamieno interno de un objeto.
"""

class Persona():
    #Propiedades, caracteristicas o atributos
    apellidos = ""
    nombres = ""
    edad = 0
    despierta = False

    #Funcionalidades

    def despertar(self):
        # self : Parametro que hace referencia a la instancia perteneciente a la clase
        self.despierta = True
        print("Buen dia")

persona1 = Persona()
persona1.apellidos="Alcivar"
print(persona1.apellidos)
persona1.despertar()
print(persona1.despierta)

print("\n")

persona2 = Persona()
persona2.apellidos="Parraga"
print(persona2.apellidos)
print(persona2.despierta)