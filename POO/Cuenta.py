class Cuenta():

    def __init__(self,pro,sal,mon):
        self.__propietario = pro
        self.__saldo = sal
        self.__moneda = mon

    # Getters / Obtiene Valores
    def get_Saldo(self):
        return self.__saldo

    def get_Propietario(self):
        return self.__propietario

    def get_Moneda(self):
        return self.__moneda

    # Setters / Modificar Valores

    def set_Moneda(self,moneda):
        self.__moneda = moneda


cuenta1 = Cuenta("Dali Alcivar", 15000 , "Dolar")
print(cuenta1.get_Saldo())
print(cuenta1.get_Moneda())
cuenta1.set_Moneda("Euro")
print(cuenta1.get_Moneda())