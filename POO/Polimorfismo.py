"""
Polimorfismo (poli = muchas / morfos = formas)
"""

class Estudiante():

    def describir(self):
        print("Soy buen estudiante")


class Docente():

    def describir(self):
        print("Me dedico a enseñar")


class Trabajador():

    def describir(self):
        print("Trabajo en un agran empresa")


def describirPersona(persona):
    persona.describir()

doc1 = Docente()
describirPersona(doc1)
