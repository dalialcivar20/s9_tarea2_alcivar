"""
Paquetes: Son directorios(carpetas) donde se almacenan modulos relacionados entre si

SIRVEN PARA: Organizar mejor el codigo y poder reutilizarlo mejor (modularizacion y reutilizacion)

SE CREA: Creando una carpeta o directorio con un archivo dentro llamado __init__.py

FUNCION DEL __INIT__.PY : Convierte un directorio en modulo (paquete) que contiene
otros modulos y esto lo hace para poder importarlos.
"""

from Paquete1.funcionesCadena import contarLetras
from Paquete1.funcionesNumericas import *

print(multiplicar(5,6))
print(contarLetras("DaliAlcivar"))